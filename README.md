# F103 EXPANSION BOARD

Low cost expansion board for STM32F103C8T6 kit, as known as [Blue pill](https://wiki.stm32duino.com/index.php?title=Blue_Pill).

License: [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). 
![CC-BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)


## Possible experiments (no purchasing required)

* GPIO (In/Out/Interruptions)
* ADC (Using variable resistor or NTC temperature sensor)
* PWM (Using leds for visualization)
* SPI (Serial Flash)
* I2C (EEPROM)
* RTC
* Timers
* USB
* USART (CMOS level)


## Possible experiments (purchasing required)

* LCD (via IO expander or I2C and additional LCD)
* USART (using additional UART/USB adapter)
* CAN (transceiver required)


## Estimated cost

| Part Number		| Part Number Mouser	| Quant	| Price (US$)	| Total (US$)	| Link                 |
|-----------------------|-----------------------|-------|---------------|---------------|----------------------|
| RV1			| 3296Y-11-103LF	| 1	| 1,40		| 1,40		| https://is.gd/fpUKY4 |
| TH1			| NK103C1R1		| 1	| 1,20		| 1,20		| https://is.gd/sfXSpN |
| BT1			| BAT-HLD-012-SMT	| 1	| 0,29		| 0,29		| https://is.gd/8aQoof |
| D4			| APTD3216LSURCK	| 1	| 0,31		| 0,31		| https://is.gd/tObJTc |
| D1,D2,D3		| APTD3216LCGCK		| 3	| 0,33		| 0,99		| https://is.gd/fdu3X9 |
| J1, J2		| 801-87-020-10-001101	| 2	| 1,45		| 2,90		| https://is.gd/hfGMlB |
| J4,J5			| 67997-410HLF		| 4	| 0,19		| 0,76		| https://is.gd/U8Usdg |
| J3			| XG8V-0831		| 1	| 0,50		| 0,50		| https://is.gd/ncAOJw |
| R1,R2			| AC1206FR-102KL	| 2	| 0,06		| 0,11		| https://is.gd/NDVNuv |
| R4,R5,R6,R7		| AC1206FR-10390RL	| 4	| 0,06		| 0,22		| https://is.gd/g12LR5 |
| R3			| AC1206FR-1010KL	| 1	| 0,06		| 0,06		| https://is.gd/lXaAps |
| SW1,SW2,SW3,SW4,SW5	| EVQ-Q2F01W		| 5	| 0,28		| 1,38		| https://is.gd/BbuHu2 |
| U1			| AT24CS01-SSHM-B	| 1	| 0,19		| 0,19		| https://is.gd/SHz4Vz |
| U3			| TCA9534DWR		| 1	| 1,07		| 1,07		| https://is.gd/log03c |
| U4			| W25Q80DVSNIG		| 1	| 0,42		| 0,42		| https://is.gd/KLfBCq |
| CR1216 Battery        | Mouser                | 1     | 1,00          | 1,00          |                      |
| Blue Pill             | AliExpress            | 1     | 2,00          | 2,00          |                      |
| PCB                   | Seeed Studio          | 1     | 2,00          | 2,00          |                      |
|-----------------------|-----------------------|-------|---------------|---------------|----------------------|
|			|			|	| Total (US$)	| 16,76 	|		       |

## Schematic

[Schematic in PDF](https://bitbucket.org/marcelobarrosalmeida/kit_f103/raw/bc04dc88c67b0578651c4d4240c09494a2e41ee0/hardware/v0/kit_f103/images/schematic.pdf).

![Top3D](https://bitbucket.org/marcelobarrosalmeida/kit_f103/raw/bc04dc88c67b0578651c4d4240c09494a2e41ee0/hardware/v0/kit_f103/images/top3d.png)
![Top](https://bitbucket.org/marcelobarrosalmeida/kit_f103/raw/bc04dc88c67b0578651c4d4240c09494a2e41ee0/hardware/v0/kit_f103/images/top.png)
![Bot](https://bitbucket.org/marcelobarrosalmeida/kit_f103/raw/bc04dc88c67b0578651c4d4240c09494a2e41ee0/hardware/v0/kit_f103/images/bot.png)
